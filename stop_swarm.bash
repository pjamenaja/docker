#!/bin/bash

docker swarm leave --force

docker rm -f $(sudo docker ps -aq)
docker volume rm -f $(sudo docker volume ls -q)
