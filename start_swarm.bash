#!/bin/bash

export DB_NAME=onix.dev.wtt.development
export NFS_IP=10.140.0.2
#export MODE=local
export MODE=nfs

export ROOT_MOUNT=/wtt
#export ROOT_MOUNT=/home/seubpong/mount

export DEV_MOUNT=/home/pjame_fb/develop

#for local
export DAT_MOUNT=${ROOT_MOUNT}/${DB_NAME}/pgdata
export WEB_MOUNT=${ROOT_MOUNT}/${DB_NAME}/onix

docker swarm init
docker stack deploy -c app_onix_${MODE}.yml onix

