#!/bin/bash

#docker run -d -p 443:443 -p 80:80 --name onix_test pjamenaja/httpd_php:1.1

DB_NAME=onix.dev.wtt.development
DB_PASSWORD=${DB_NAME}x
DB_NETWORK=onix_postgres_nw

NFS_IP=10.140.0.2
ROOT_MOUNT=/wtt

WEB_MOUNT=volume_onix
DAT_MOUNT=volume_pgdata

docker volume create --opt type=nfs \
--opt device=:${ROOT_MOUNT}/onix \
--opt o=addr=${NFS_IP},rsize=1024,wsize=1024 \
--name ${WEB_MOUNT}

docker volume create --opt type=nfs \
--opt device=:${ROOT_MOUNT}/pgdata \
--opt o=addr=${NFS_IP},rsize=1024,wsize=1024 \
--name ${DAT_MOUNT}

#docker volume create --name ${WEB_MOUNT}

#docker volume create --name ${DAT_MOUNT}

docker network create ${DB_NETWORK} 

#Bind mount
#WEB_MOUNT=/home/pjame_fb/nfs/${DB_NAME}/onix
#DAT_MOUNT=/home/pjame_fb/nfs/${DB_NAME}/pgdata
#DEV_MOUNT=/home/pjame_fb/nfs/${DB_NAME}/develop

#docker run -d -p 443:443 --name onix_web --volume ${WEB_MOUNT}:/onix pjamenaja/httpd_php:1.3

docker run -d -p 443:443 --name app_onix \
--network ${DB_NETWORK} \
--volume ${WEB_MOUNT}:/onix pjamenaja/app_onix:1.1
#--volume ${DEV_MOUNT}:/wis \

docker run -d --name onix_postgres --network ${DB_NETWORK} \
--user onix:onix --volume ${DAT_MOUNT}:/pgdata:nocopy \
pjamenaja/postgresql:1.0
#--user onix:onix --volume ${DEV_MOUNT}:/wis \

