#!/bin/bash

docker rm -f app_onix
docker rm -f onix_postgres

docker network rm onix_postgres_nw
docker volume rm volume_onix
docker volume rm volume_pgdata

